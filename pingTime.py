#!/usr/bin/python
#
# This appends ping data to a file.
#
# Seth McNeill
# 2015 October 18

import subprocess
import time
import sys
import re
import os
import argparse
import datetime
import jwt # for javascript web token library (PyJWT)
import creds # file contains pubKey and privateKey variables (see README.md)
import requests # for https calls
import json # to display json in a pretty way
from flatten_dict import flatten # to flatten dict of data prior to submission
import csv # to read parameters and read/write failed pings
import pdb # debugging

filename = "junk.txt"
nPings = 1
site = "google.com"
# start the regular expressions
regC = re.compile('([0-9]+) packets transmitted, ([0-9]+) received, ([0-9]+)% packet loss, time ([0-9]+)ms'+ '\nrtt min/avg/max/mdev = ([0-9.]+)/([0-9.]+)/([0-9.]+)/([0-9.]+) ms')
regFail = re.compile('100% packet loss')

def run_ping(site,nPings):
  print("Running: ping -c" + str(nPings) + " -D " + site)
  ping_response = subprocess.Popen(["/bin/ping", "-c"+str(nPings), "-D", "-O", site], stdout=subprocess.PIPE).stdout.read().decode('utf-8')
  if(regFail.search(ping_response)):
    print("ping failed for " + site)
    return None
  else:
    return ping_response

def save_raw_ping(ping_response, filename, ts):
  f = open(filename, 'a')
  f.write(str(ts) + "\n")
  f.write(ping_response)
  f.write("\n\n\n")
  f.close()

def save_csv(ping_response, filename, ts):
  m = regC.search(ping_response)
  csvFile = filename + ".csv"

  if(not(os.path.isfile(csvFile))): # csv does not exist
    f = open(csvFile, 'w')
    f.write("date, nxmit, nrcv, pctLoss, time, min(ms), avg(ms), max(ms), mdev(ms)\n")
    f.close()

  f = open(csvFile, 'a')
#  t = (ts - datetime.datetime(1970, 1, 1, tzinfo=datetime.timezone.utc)).total_seconds()
  f.write(str(ts) + ',' + m.group(1) + ',' + m.group(2) + ',' + m.group(3) + ',' + m.group(4) + ',' + 
    m.group(5) + ',' + m.group(6) + ',' + m.group(7) + ',' + m.group(8) + '\n')
  f.close()

def send_jwt(ping_response, ts, pingURL, jwt_id, jwtURL):
  sendData = {}
  sendData['ts'] = (ts - datetime.datetime(1970, 1, 1, tzinfo=datetime.timezone.utc)).total_seconds()
  sendData['id'] = jwt_id
  sendData['site'] = pingURL
  if(ping_response is None):
    print('ping failed for ' + pingURL)
    sendData['pctLoss'] = 100.0
  else:
    m = regC.search(ping_response)
    sendData['nxmit'] = m.group(1)
    sendData['nrcv'] = m.group(2)
    sendData['pctLoss'] = m.group(3)
    sendData['time'] = m.group(4)
    sendData['min'] = m.group(5)
    sendData['avg'] = m.group(6)
    sendData['max'] = m.group(7)
    sendData['mdev'] = m.group(8)

  print(json.dumps(sendData, indent=2))
  encoded = jwt.encode(sendData, creds.privateKey, algorithm='RS256')
  # submit data
  response = requests.post(jwtURL, data={"data":encoded})
  print(f"Website responded:\n{response.text}")


if __name__ == '__main__':
  startTime = datetime.datetime.now()
  parser = argparse.ArgumentParser(description="Sends internet speeds to the web using JWT")
  # Good discussion on require/optional 
  # https://stackoverflow.com/questions/24180527/argparse-required-arguments-listed-under-optional-arguments
  required = parser.add_argument_group('required arguments') # still requires ', required=True' argument
  optional = parser.add_argument_group('optional arguments')
  required.add_argument('-i', '--id', help='The ID of the device on the server', required=True)
  optional.add_argument('-j', '--jwt', help='The URL of the JWT data receiver')
  optional.add_argument('-o', '--outfile', help='Filename of output, default is: ' + filename)
  optional.add_argument('-n', '--npings', help='Number of pings to use, default is: ' + str(nPings))
  optional.add_argument('-u', '--url', help='URL to ping, default is: ' + site)
  optional.add_argument('-p', '--parameters', help='A file containing the parameters to use')
  optional.add_argument('-f', '--fails', help='The filename of a file that lists the failed pings since last successful ping')
  optional.add_argument('--debug', action='store_true', help="Set the program in debug mode")
  
  args = parser.parse_args()
  if(args.outfile):
    filename = args.outfile
  if args.debug:
    debugMode = True
  else:
    debugMode = False
  if(not args.parameters):
    if(args.npings):
      nPings = int(args.npings)
    if(args.url):
      site = args.url
    ts = datetime.datetime.now(datetime.timezone.utc)
    ping_response = run_ping(site, nPings)

    if(args.outfile):
      save_raw_ping(ping_response, filename, ts)
      save_csv(ping_response, filename, ts)

    if(args.jwt): # send data via jwt to storage server
      send_jwt(ping_response, ts, site, args.id, args.jwt)
  else:
    with open(args.parameters) as csvfile:
      reader = csv.DictReader(csvfile)
      for row in reader:
        ts = datetime.datetime.now(datetime.timezone.utc)
        ping_response = run_ping(row['pingURL'], row['npings'])
        send_jwt(ping_response, ts, row['pingURL'], row['ID'], row['jwtURL'])

  endTime = datetime.datetime.now()
  print(f'{__file__} took {endTime - startTime}')
