# Usage
Typically this is run by adding a line to crontab -e as follows:
7 * * * * /home/pi/projects/pingTimes/pingTime.py /home/pi/projects/pingTimes/google.txt 50 google.com

# PyJWT usage link
https://github.com/jpadilla/pyjwt/blob/master/docs/usage.rst

# Generate credentials
https://gist.github.com/ygotthilf/baa58da5c3dd1f69fae9
`ssh-keygen -t rsa -b 4096 -m PEM -f jwtRS256.key`
Convert the key to PEM format with:   
`openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub`
Be sure to upload the public key to the server

# creds.py
This file contains two variables: 
`privateKey = b'''-----BEGIN RSA PRIVATE KEY-----\nMIIE...-----END RSA PRIVATE KEY-----'''`
`publicKey = b'''-----BEGIN RSA PUBLIC KEY-----\n...-----END RSA PUBLIC KEY-----'''`

# Parameter file
This is a csv file with the following columns:
- ping URL - The site to ping
- Number of pings
- JWT URL - The website to send the results to
- device ID - corresponds to public key on the server

# Common Errors
## OpenSSL error: error:0909006C:PEM routines:get_name:no start line
It means that the public key in PEM format was not generated as mentioned above or at:
https://gist.github.com/ygotthilf/baa58da5c3dd1f69fae9


